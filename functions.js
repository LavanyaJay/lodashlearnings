var _ = require('lodash');

//Generate random numbers
//Using native js

console.log(Math.random());

//random number between two numbers
function random(from, to) {
  return Math.floor(Math.random() * to) + from;
}
console.log(random(1, 10));

//Random number using Lodash

console.log(_.random(1, 100, true));

//Generate unique ID for posts etc

console.log(_.uniqueId('user_')); //using prefix
console.log(_.uniqueId(''));

//Flatten the array
let no = [
  [1, 2],
  [3, 4],
];
console.log(_.flatten(no));

//Remove unwanted the properties from an array
let nos = [1, 2, null, 3, undefined, false, '', 4, 5];

//Using native js filter
console.log(nos.filter(Boolean));

//Using Lodash
console.log(_.compact(nos));

//Merge objs together

let state = {
  isLoading: true,
  data: null,
  err: null,
};

let newState = {
  isLoading: false,
  data: { id: 1, name: 'John' },
};

//Using native js
let merge = Object.assign({}, state, newState); //Obj.assign does not work in all browsers
console.log(merge);

//Using lodash - cross browser support, can be used in chains
console.log(_.merge(state, newState));

//cloning objects
let baseConfig = {
  url: 'http:/someapi.com',
  port: 4000,
};

/* function extendedConfigFn(config) {
  //config.host = 'http://google.com';
  return Object.assign({}, config, { host: 'http://google.com' });
}

let extendedConfig = extendedConfigFn(baseConfig); */
/* console.log(baseConfig);
console.log(extendedConfig); */

//Cloning object in lodash
let clonedConfig = _.clone(baseConfig);
clonedConfig.host = 'http://google.com';
console.log(baseConfig);
console.log(clonedConfig);

//Lodash clone not work on nested object, use cloneDeep but it is performance costly
let a = { b: { c: 1 } };
let b = _.cloneDeep(a);
b.b.foo = 'bar';
console.log(a);
console.log(b);

//Is Methods (checking types)(Read more from documation)

console.log(_.isEqual(1, '1'));
console.log(_.isEqual({ a: 1 }, { a: 1 }));
console.log(_.isEmpty({}));
console.log(_.isEmpty([]));
console.log(_.isEmpty(null)); //this is the pitfall

//To check undefined null
let user = null;
console.log(!_.isNil(user) ? console.log(user) : null);

//Debounce  => control on how a fn is called (to reduce the load for evey key change)
//_.debounce(handler,2000)

//Throttle => _.throttle(handler,2000)

//Mixins - using our own methods inside lodash chain

function capitalizeLast(str) {
  let lastChar = _.last(str);
  let initialChars = _.chain(str).initial().join('').value();
  return initialChars + _.capitalize(lastChar);
}
console.log(capitalizeLast('foo'));

_.mixin({ capitalizeLast: capitalizeLast });
console.log(_.chain('foo').capitalizeLast().value());
