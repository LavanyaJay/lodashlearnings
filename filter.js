var _ = require('lodash');
console.log(_.filter([1, 2, 3, 4, 5], (item) => item < 3));

//Filter Array
let users = [
  { id: 1, first_name: 'John', status: 'active' },
  { id: 2, first_name: 'Peter', status: 'inactive' },
  { id: 3, first_name: 'Lavan', status: 'active' },
];

//With and without syntatic sugar
console.log(_.filter(users, (user) => user.first_name === 'John'));
console.log(_.filter(users, { first_name: 'John' }));

//Filter Object
var items = {
  1: { name: 'milk' },
  2: { name: 'bread' },
};
console.log(_.filter(items, { name: 'milk' }));

//Filter search
var products = [
  { id: 1, name: 'milk', price: '1$' },
  { id: 2, name: 'bread', price: '2$' },
  { id: 3, name: 'meat', price: '1.50$' },
];

function search(productsArray, searchValue) {
  return _.filter(productsArray, (product) => {
    return _.includes(product.name, searchValue);
  });
}
console.log(search(products, 'm'));
