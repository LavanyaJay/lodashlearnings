//forEach.js
var _ = require('lodash');

var native = [1, 2].forEach(function (item) {
  console.log(item);
});
var lodash = _.forEach([1, 2], function (item) {
  return item++;
});

console.log(native); //undefined
console.log(lodash);

var items = {
  1: { name: 'milk' },
  2: { name: 'bread' },
};
let array = [];
_.forEach(items, function (item) {
  array.push(item);
});

console.log(array);
console.log(Object.values(items));
