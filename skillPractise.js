var _ = require('lodash');
let loc = [
  {
    location_key: [30, 22, 44],
    autoassign: 1,
  },
  {
    location_key: [55, 62, 41],
    autoassign: 2,
  },
];

let bulkConfig = [
  {
    dataValues: {
      config_key: 100,
    },
  },
  {
    dataValues: {
      config_key: 200,
    },
  },
];
//End Result [{config_key:100,location_key:30,autoassign:1},{...},...]
//Steps to proceed
//Iterate location array with map
//To get the index of the current element
//Use the index to get the config key for the object
//and create an object for each element in the array, flatten the array

if (_.isEqual(loc.length, bulkConfig.length)) {
  /*   console.log(
    _.flatten(
      _.map(loc, (locItem, index) => {
        return _.map(locItem.location_key, (key) => {
          return {
            location_key: key,
            config_key: bulkConfig[index].dataValues.config_key,
            autoassign: locItem.autoassign,
          };
        });
      })
    )
  ); */
  //Making it more readable and code friendly
  function getConfigs(locItem, index) {
    return _.map(locItem.location_key, (key) => {
      return {
        location_key: key,
        config_key: bulkConfig[index].dataValues.config_key,
        autoassign: locItem.autoassign,
      };
    });
  }
  let result = _.chain(loc).map(getConfigs).flatten().value();
  console.log(result);
}

//To implement basket functionality in e-commerce shop

let basket = [];
function addProduct(basket, objItem) {
  return _.concat(basket, objItem);
}
basket = addProduct(basket, { id: 1, name: 'Milk', amount: 1, price: 2 });

basket = addProduct(basket, { id: 2, name: 'Bread', amount: 1, price: 3 });
console.log(basket);

//[{ id: 1, name: 'Milk', amount: 1, price: 2 }, { id: 2, name: 'Bread', amount: 1, price: 3 }]

function increaseAmount(basket, id) {
  return _.map(basket, (item) => {
    item.id === id ? item.amount++ : null;
    return item;
  });
}
basket = increaseAmount(basket, 1);
console.log(basket);
//[{ id: 1, name: 'Milk', amount: 2, price: 2 }, { id: 2, name: 'Bread', amount: 1, price: 3 }]

function decreaseAmount(basket, id) {
  return _.map(basket, (item) => {
    item.id === id ? item.amount-- : null;
    return item;
  });
}
basket = decreaseAmount(basket, 1);
console.log(basket);
//[{ id: 1, name: 'Milk', amount: 1, price: 2 }, { id: 2, name: 'Bread', amount: 1, price: 3 }]

function removeById(basket, id) {
  return _.reject(basket, { id });
}
basket = removeById(basket, 1);
console.log(basket);
//[{ id: 2, name: 'Bread', amount: 1, price: 3 }]

function getTotal(basket) {
  return _.reduce(
    basket,
    (total, item) => {
      return total + item.amount;
    },
    0
  );
}
let total = getTotal(basket);
console.log(total);
//1

function getTotalPrice(basket) {
  return _.reduce(
    basket,
    (total, item) => {
      return total + item.price;
    },
    0
  );
}
let totalPrice = getTotalPrice(basket);
console.log(totalPrice);
//3

//Steps
//Define addProduct function to add one item to the basket
//Add multiple products to the basket
//Add increaseAmount for a basket item
//Add decreaseAmount for a basket item
//Remove a item from the basket
//Get total price of all the items in the basket
