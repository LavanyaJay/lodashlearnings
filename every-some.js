var _ = require('lodash');

//To check any condition for all items in an array _every is useful
//Check if all users are active
let users = [
  {
    id: 1,
    name: 'John',
    isActive: true,
  },
  {
    id: 2,
    name: 'Peter',
    isActive: false,
  },
  {
    id: 3,
    name: 'Jack',
    isActive: false,
  },
];
console.log(_.every(users, { isActive: true }));

//To check if atleast one item in an array adhere to a condition -> _some is useful
//Check if atleast one user is active

console.log(_.some(users, { isActive: true }));
