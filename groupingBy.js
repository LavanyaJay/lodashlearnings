var _ = require('lodash');

let users = [
  {
    name: 'John',
    isActive: true,
  },
  {
    name: 'Jack',
    isActive: false,
  },
  {
    name: 'Peter',
    isActive: true,
  },
];

//Group all active and inactive users
//Using native JS

function groupBy(users, prop) {
  return users.reduce((total, element) => {
    (total[element[prop]] = total[element[prop]] || []).push(element);
    return total;
  }, {});
}
console.log(groupBy(users, 'isActive'));

//Group the above using lodash
console.log(_.groupBy(users, 'isActive'));
