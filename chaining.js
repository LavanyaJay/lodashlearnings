var _ = require('lodash');

let users = [
  {
    id: 1,
    name: 'John',
    age: 40,
    isActive: true,
  },
  {
    id: 2,
    name: 'Peter',
    age: 21,
    isActive: false,
  },
  {
    id: 3,
    name: 'Jack',
    age: 25,
    isActive: true,
  },
  {
    id: 4,
    name: 'Alex',
    age: 23,
    isActive: true,
  },
];
//Get youngest active user
const getYoungestUser = (users) => {
  let activeUsers = _.filter(users, { isActive: true });
  let sortUsers = _.orderBy(activeUsers, 'age');
  let result = sortUsers[0];
  return `${result.name} is the youngest user whose age is ${result.age}`;
};

console.log(getYoungestUser(users));

//Using chain method (POWER OF LODASH)
const getYoungestUserNew = (users) => {
  return _.chain(users)
    .filter('isActive')
    .orderBy(['age'])
    .map((user) => {
      return `${user.name} is ${user.age}`;
    })
    .head() //to get to the first element of the array
    .value(); //to unwrap the values in chain
};
console.log(getYoungestUserNew(users));
