var _ = require('lodash');

let a = [1, 2, 3, 4, 5];

//To get the first element of the array
console.log(_.head(a));

//To get the first character of the string
console.log(_.head('Lavan'));

//To get all elements except first element of the array
console.log(_.tail(a));
console.log(a.slice('1')); //same as slice.. but slice is not readable

//To get all characters of the string except the first one
console.log(_.chain('Lavan').tail().join('').value());

//To get the last element of the array
console.log(_.last(a));

//To get the last character of the string
console.log(_.last('Lavan'));

//To get all elements of the array except the last one
console.log(_.initial(a));

//To get all characters of the string except the last one
console.log(_.chain('lavan').initial().join('').value());
