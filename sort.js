var _ = require('lodash');
//sort an array using order by in lodash

let users = [
  {
    id: 1,
    name: 'Jack',
    likes: 11,
  },
  {
    id: 2,
    name: 'John',
    likes: 11,
  },
  {
    id: 3,
    name: 'Kyle',
    likes: 9,
  },
];
//sort using native js
console.log(
  users.sort(function (user1, user2) {
    return user1.likes > user2.likes;
  })
);

//sort using orderBy Lodash (asc)
console.log(_.orderBy(users, 'likes'));

//sort using orderBy Lodash (desc)
console.log(_.orderBy(users, ['likes', 'name'], ['desc', 'asc']));
