var _ = require('lodash');
var array = _.map(
  [
    { id: 1, name: 'milk' },
    { id: 2, name: 'bread' },
  ],
  'id'
);
console.log(array);

let items = {
  1: { name: 'milk' },
  2: { name: 'bread' },
};

let things = _.map(items, 'name');
console.log(things);

let users = [
  { id: 1, first_name: 'John', status: 'active' },
  { id: 2, first_name: 'Peter', status: 'inactive' },
  { id: 3, first_name: 'Lavan', status: 'active' },
];

function normalizeUsers(usersArray) {
  return _.map(usersArray, (user) => {
    return {
      id: user.id,
      firstName: user.first_name,
      isActive: user.status === 'active',
    };
  });
}
console.log(normalizeUsers(users));
