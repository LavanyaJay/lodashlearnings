var _ = require('lodash');

//To change str to uppercase
console.log(_.toUpper('lavan'));

//To change str to uppercase
console.log(_.toLower('LAVAN'));

//Split in a chain
console.log(_.chain('LAVAN').toLower().split('').value());

//Join and split
console.log(_.chain('FOO/BAR').split('/').head().value());

//To concatenate array
console.log(_.join(['foo', 'bar'], '/'));
//To change str to uppercase
console.log(_.chain(['foo', 'bar']).join('/').toUpper().value());

//To convert string to slug

function toSlug(str) {
  return _.chain(str).split(' ').join('-').toLower().value();
}
console.log(toSlug('This is a super quiz'));

//Caplitalize

function caplitalize(str) {
  let firstLetter = _.chain(str).toLower().head().toUpper().value();
  let tailLetters = _.chain(str).toLower().tail().join('').value();
  return firstLetter + tailLetters;
}
console.log(caplitalize('foO'));

//Camelcase and Snakecase
function camelCase(str) {
  return _.camelCase(str);
}

function snakeCase(str) {
  return _.snakeCase(str);
}

console.log(camelCase('Foo-Bar baz'));
console.log(snakeCase('Foo-Bar baz'));
