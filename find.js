var _ = require('lodash');

let users = [
  {
    id: 1,
    name: 'Peter',
    isActive: true,
  },
  {
    id: 2,
    name: 'John',
    isActive: false,
  },
  {
    id: 3,
    name: 'Jack',
    isActive: true,
  },
];

console.log(_.find(users, { name: 'John' })); //find returns only the first element found

//Remove element from array without condition - No mutation of original data
let array1 = [1, 2, 3, 4, 5, 6, 7, 8, 9];
console.log(_.without(array1, 1, 2, 3));
console.log(array1);

//Remove element from an array with condition - Mutation of original data (Filter does not mutate original data, use it rarely!)
console.log(_.remove(users, { isActive: false }));
console.log(users);

//Reject unwanted elements without mutation (helps to avoid negation used in filter)
console.log(_.reject(users, { id: 1 }));
console.log(users);

//Get popular users (active and likes > 100)
let newUsers = [
  {
    id: 1,
    name: 'John',
    isActive: true,
    likes: 110,
  },
  {
    id: 2,
    name: 'Jack',
    isActive: false,
    likes: 90,
  },
  {
    id: 3,
    name: 'Peter',
    isActive: true,
    likes: 100,
  },
];
//Approach 1 using reject (involved negation check)
/* function getPopularUsers(usersArray) {
  return _.reject(usersArray, (user) => {
    return user.isActive === false || user.likes <= 100;
  });
} */
//Approach 2 using filter (eliminates negation check). This approach is better here!
function getPopularUsers(usersArray) {
  return _.filter(usersArray, (user) => {
    return user.Active || user.likes > 100;
  });
}
console.log(getPopularUsers(newUsers));
